from django.urls import path
from . import views

app_name = 'web'

urlpatterns = [
    path('', views.index, name='index'),
    path('story7/', views.javascript, name='javascript'),
    path('caribuku/', views.cari, name='cari'),
    path('login/', views.logIn, name='logIn'),
    path('signIn/', views.signIn, name='signIn'),
    path('logout/', views.logOut, name='logOut'),
]