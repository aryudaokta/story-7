from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.apps import apps
from .models import Akun
from .forms import Login, SignIn
from .apps import WebConfig

class Index(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("web:index"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("web:index"))
        self.assertTemplateUsed(response,'index.html')
    def test_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

class Javascript(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("web:javascript"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("web:javascript"))
        self.assertTemplateUsed(response,'javascript.html')
    def test_url(self):
        response = Client().get('/story7')
        self.assertEquals(response.status_code, 301)

class Cari(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("web:cari"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("web:cari"))
        self.assertTemplateUsed(response,'ajax.html')
    def test_url(self):
        response = Client().get('/caribuku')
        self.assertEquals(response.status_code, 301)

class Login(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("web:logIn"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("web:logIn"))
        self.assertTemplateUsed(response,'login.html')
    def test_url(self):
        response = Client().get('/login')
        self.assertEquals(response.status_code, 301)

class SignIn(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("web:signIn"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("web:signIn"))
        self.assertTemplateUsed(response,'signin.html')
    def test_url(self):
        response = Client().get('/signIn')
        self.assertEquals(response.status_code, 301)

class LogOut(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("web:logOut"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("web:logOut"))
        self.assertTemplateUsed(response,'logout.html')
    def test_url(self):
        response = Client().get('/logout')
        self.assertEquals(response.status_code, 301)

class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("Aryuda", password="321")
        self.new_user.save()
        self.akun = Akun.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "Aryuda",
            password = "321"
        )
    def test_to_string(self):
        self.assertIn("Aryuda", str(self.new_user.akun))

class LoginAppApps(TestCase):
    def test_apps(self):
        self.assertEqual(WebConfig.name, 'web') 
        self.assertEqual(apps.get_app_config('web').name, 'web')