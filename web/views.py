from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib import messages
import requests
from .forms import SignIn, Login
import json

User = get_user_model()

def index(request):
    return render(request, 'index.html')

def javascript(request):
    return render(request, 'javascript.html')

def cari(request):
    response = {}
    return render(request, 'ajax.html', response)

def ajax(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    q = requests.get(url)
    data = json.loads(q.content)
    return JsonResponse(data, safe=False)

def logIn(request):
    form = Login(request.POST or None)
    response = {
        'form' : form,
        'akun' : False,
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            response = {
                'form' : form,
                'akun' : True,
            }
            return render(request, "login.html", response)
    return render(request, "login.html", response)

def signIn(request):
    form = SignIn(request.POST or None)
    response = {
        'form' : form,
        'uname' : False,
        'sandi' : False,
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        password_check = form.cleaned_data.get("password_check")
        try:
            if password != password_check:
                response = {
                    'form' : form,
                    'uname' : False,
                    'sandi' : True,
                }
                return render(request, "signin.html", response)
            else:
                User.objects.create_user(username, email, password)
                user = authenticate(username=username, password=password)
                login(request, user)
                return redirect('/')
        except:
            if password != password_check:
                response = {
                    'form' : form,
                    'uname' : True,
                    'sandi' : True,
                }
                return render(request, "signin.html", response)
            else:
                response = {
                    'form' : form,
                    'uname' : True,
                    'sandi' : False,
                }
                return render(request, "signin.html", response)
    return render(request, "signin.html", response)

def logOut(request):
    logout(request)
    return render(request, 'logout.html')